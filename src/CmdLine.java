

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.lang.System;

/**
 * This class is used to parse command line arguments and launch appropriate commands
 *
 * Recognize the following command line options:
 * 
 * -program somefile.rbt: load the Program from the specified file
 * -world somefile.wrl: load the World description from the specified file
 * -run: run the given Program on the given World
 * -print: prints world description on System.out
 * -write somefile.wrl: save the current world to the file
 * -shell: run the interactive shell
 * -gui: start the graphical user interface
 * -help: print brief instructions
 * 
 * If only -program and -world options are provided, start the GUI anyway.
 */

public class CmdLine {
	/**
	 * parse command line options
	 */

	private World world;
	private Program program;


	public void print() {
		System.out.println("");
		System.out.println("");
		System.out.println("");
		int w=world.getWidth();
		int h=world.getHeight();
		//faccio corrispondere i numeri a lettere
		int o=world.getUlisseOrientation();
		char ori;
		boolean d=world.getUlisseDied();
		if (d==true){
			ori='+';
		} else if (o==0){
			ori='>';
		} else if (o==1){
			ori='^';
		} else if(o==2){
			ori='<';
		} else {
			ori='v';
		}
		int ux=world.getUlisseX();
		int uy=world.getUlisseY();
		int t=0;
		while (t<w+1){
			//stampa il "tetto"
			if (t==0){
				System.out.print(".");
				t++;
			}else if (t==w){
				System.out.println("_____.");
				t++;
			}

			else {
				System.out.print("_____.");
				t++;
			}
		}

		int j=h-1;
		while (j>-1){
			//i1 è l'ascissa
			int i1=-1;
			while (i1<w){
				if (i1==-1){
					System.out.print("|");

				}
				else if (i1==w-1){

					if (world.getNDiamondsInCell(i1, j)==0){
						if (ux==i1 && uy==j){
							System.out.println("  "+ ori +"  |");

						}else {
							System.out.println("     |");

						}
					}else if (world.getNDiamondsInCell(i1, j)>0 && world.getNDiamondsInCell(i1, j)<10){
						if (ux==i1 && uy==j){
							System.out.println(" "+ ori + world.getNDiamondsInCell(i1, j)+" |");

						}else {
							System.out.println("  "+ world.getNDiamondsInCell(i1, j) + "  |");

						}
					}else if (world.getNDiamondsInCell(i1, j)>9 && world.getNDiamondsInCell(i1, j)<100){
						if (ux==i1 && uy==j){
							System.out.println(" "+ ori + world.getNDiamondsInCell(i1, j)+"|");

						}else {
							System.out.println("  "+ world.getNDiamondsInCell(i1, j) + " |");

						}
					}else if (world.getNDiamondsInCell(i1, j)>99){
						if (ux==i1 && uy==j){
							System.out.println(" "+ ori + "99|");

						}else {
							System.out.println(" 99 |");

						}
					}
				}

				else if (world.isVerticalWall(i1, j)==true){
					if (world.getNDiamondsInCell(i1, j)==0){
						if (ux==i1 && uy==j){
							System.out.print("  "+ ori +"  |");

						}else {
							System.out.print("     |");

						}
					}else if (world.getNDiamondsInCell(i1, j)>0 && world.getNDiamondsInCell(i1, j)<10){
						if (ux==i1 && uy==j){
							System.out.print(" "+ ori + world.getNDiamondsInCell(i1, j)+" |");

						}else {
							System.out.print("  "+ world.getNDiamondsInCell(i1, j) + "  |");

						}
					}else if (world.getNDiamondsInCell(i1, j)>9 && world.getNDiamondsInCell(i1, j)<100){
						if (ux==i1 && uy==j){
							System.out.print(" "+ ori + world.getNDiamondsInCell(i1, j)+"|");

						}else {
							System.out.print("  "+ world.getNDiamondsInCell(i1, j) + " |");

						}
					}else if (world.getNDiamondsInCell(i1, j)>99){
						if (ux==i1 && uy==j){
							System.out.print(" "+ ori + "99|");

						}else {
							System.out.print(" 99 |");

						}
					}

				}else if (world.isVerticalWall(i1, j)==false){
					if (world.getNDiamondsInCell(i1, j)==0){
						if (ux==i1 && uy==j){
							System.out.print("  "+ ori +"   ");

						}else {
							System.out.print("      ");

						}
					}else if (world.getNDiamondsInCell(i1, j)>0 && world.getNDiamondsInCell(i1, j)<10){
						if (ux==i1 && uy==j){
							System.out.print(" "+ ori + world.getNDiamondsInCell(i1, j)+"  ");

						}else {
							System.out.print("  "+ world.getNDiamondsInCell(i1, j) + "   ");

						}
					}else if (world.getNDiamondsInCell(i1, j)>9 && world.getNDiamondsInCell(i1, j)<100){
						if (ux==i1 && uy==j){
							System.out.print(" "+ ori + world.getNDiamondsInCell(i1, j)+" ");

						}else {
							System.out.print("  "+ world.getNDiamondsInCell(i1, j) + "  ");

						}
					}else if (world.getNDiamondsInCell(i1, j)>99){
						if (ux==i1 && uy==j){
							System.out.print(" "+ ori + "99 ");

						}else {
							System.out.print(" 99  ");

						}
					}
				}i1++;
			}

			//i2 è l'ascissa, come prima era t e come dopo è p
			int i2=-1;
			if (j==0){
				break;

			}
			else {
				while (i2<w){

					if (i2==-1){
						System.out.print(".");

					}else if (world.isHorizontalWall(i2, j-1)==true){
						System.out.print("_____.");


					}else {
						System.out.print("     .");

					}i2++;

				}
			}
			System.out.println("");
			j--;
		}
		int p=0;
		while (p<w+1){
			//stampa il "pavimento"
			if (p==0){
				System.out.print(".");
				p++;

			}else {
				System.out.print("_____.");
				p++;
			}
		}
		if (d==true){
			System.out.println("");
			System.out.println("Why has you killed me? :-("); 
		}
	}

	public void readWorld(String filename) {
		//definisco ora una variabile di tipo Worldreader
		WorldReader wr;
		//col try/catch provo a fare una cosa, se non sono in quel caso
		//passo alle alternative
		try {
			//qui sotto dico cosa mettere in wr
			//l'operatore new crea un oggetto dalla definizione di classe
			wr = new WorldReader(new FileReader(filename));

			wr.readWorld(world);

		} catch (ParseError e) {
			e.printStackTrace();
			//dalla parte ParseError prendo la frase da scrivere
			//mi stampa che ho trovato un errore, e a quale linea, e di quale file
			System.out.println(e.getMessage()+" Line number:"+e.getLineNumber()+" File " +
					"name:"+e.getFileName());
		}catch (FileNotFoundException e) {
			e.printStackTrace();
			//stampa la seguente frase nichilista
			System.out.println("World doesn't exist");
		}

	}

	public void readProgram(String programName){
		//dichiaro la variabile rd di tipo FileReader
		FileReader rd;
		try {
			rd = new FileReader(programName);
			Compiler compiler=new Compiler();
			// dichiariamo la variabile program di tipo Program
			program=compiler.compile(rd);
		}
		catch (ParseError e) {
			e.printStackTrace();
			//dalla parte ParseError prendo la frase da scrivere
			//mi stampa che ho trovato un errore, e a quale linea, e di quale file
			System.out.println(e.getMessage()+" Line number:"+e.getLineNumber()+" File " +
					"name:"+e.getFileName());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.out.println("program doesn't exist");
		}
	}

	public void runGame(){
		if (program==null || world==null){
			System.out.println("How do you think I can run a program that you haven't loaded in a world that doesn't exist?!Load it!!");
		}
		else {
			//fai virtual machine
			VirtualMachine virtualMachine = new VirtualMachine();
			((VirtualMachine) virtualMachine).setProgram(program);
			virtualMachine.setWorld(world);
			while (!virtualMachine.hasTerminated())
				try {
					virtualMachine.step();
				} catch (VirtualMachineError e) {

					e.printStackTrace();
				}

		}
	}

	public void createShell(){

		Tokenizer in = null;
		try {
			in = new Tokenizer(new InputStreamReader(System.in));
		} catch (ParseError e) {
			e.printStackTrace();
			//dalla parte ParseError prendo la frase da scrivere
			//mi stampa che ho trovato un errore, e a quale linea, e di quale file
			System.out.println(e.getMessage()+" Line number:"+e.getLineNumber()+" File " +
					"name:"+e.getFileName());

		}
		while(true) {
			System.out.println("$");
			String command = null;
			try {
				command = in.getIdentifier();
			} catch (ParseError e) {
				e.printStackTrace();
				//dalla parte ParseError prendo la frase da scrivere
				//mi stampa che ho trovato un errore, e a quale linea, e di quale file
				System.out.println(e.getMessage()+" Line number:"+e.getLineNumber()+" File " +
						"name:"+e.getFileName());
			}
			if (command.equals("-program")){
				//dichiaro una variabile programName di tipo String

				String programName = null;
				try {
					programName = in.getString();
				} catch (ParseError e) {
					e.printStackTrace();
					//dalla parte ParseError prendo la frase da scrivere
					//mi stampa che ho trovato un errore, e a quale linea, e di quale file
					System.out.println(e.getMessage()+" Line number:"+e.getLineNumber()+" File " +
							"name:"+e.getFileName());
				}
				readProgram(programName);

			}

			else if (command.equals("-world")){

				String filename = null;
				try {
					filename = in.getString();
				} catch (ParseError e) {
					e.printStackTrace();
					//dalla parte ParseError prendo la frase da scrivere
					//mi stampa che ho trovato un errore, e a quale linea, e di quale file
					System.out.println(e.getMessage()+" Line number:"+e.getLineNumber()+" File " +
							"name:"+e.getFileName());
				}
				readWorld(filename);

			}
			else if (command.equals("-run")){
				runGame();
			}

			else if (command.equals("-print")){
				print();
			}
			else if (command.equals("advance")){
				world.ulisseAdvance();
				print();
			}

			else if (command.equals("turnLeft")){
				world.ulisseTurnLeft();
				print();
			}

			else if (command.equals("turnRight")){
				world.ulisseTurnRight();
				print();
			}

			else if (command.equals("isFrontClear")){
				world.ulisseFrontIsClear();
				print();
			}

			else if (command.equals("takeDiamond")){
				world.ulisseTakeDiamond();
				print();
			}

			else if (command.equals("dropDiamond")){
				world.ulisseDropDiamond();
				print();
			}
			else if (command.equals("diamondIsPresent")){
				world.ulisseDiamondIsPresent();
				print();
			}
			else if (command.equals("leftIsClear")){
				world.ulisseLeftIsClear();
				print();
			}
			else if (command.equals("rightIsClear")){
				world.ulisseRightIsClear();
				print();
			}
			else if (command.equals("facingEast")){
				world.ulisseFacingEast();
				print();
			}
			else if (command.equals("facingNorth")){
				world.ulisseFacingNorth();
				print();
			}
			else if (command.equals("facingWest")){
				world.ulisseFacingWest();
				print();
			}
			else if (command.equals("facingSouth")){
				world.ulisseFacingSouth();
				print();
			}
			else if (command.equals("addDiamondInBag")){
				world.addDiamondInBag();
				print();
			}
			else if (command.equals("removeDiamondFromBag")){
				world.removeDiamondFromBag();
				print();
			}
			else if (command.equals("quit")){
				break;
			}else{
				System.out.println("unknown command");
			}
		}
		//mi apre un prompt da quale posso
		//stabilire cosa far fare a ulisse senza
		//passare dalla gui e facendo variazioni sul program
	}

	public void createWrite(String writername){
		if (writername==null){
			System.out.println("I can't save something unknown. Load some world, please!");
		}
		else{
			//salvo il mondo in un file
			//FileWriter fw= new FileWriter(filename);
			try {
				FileWriter wwrite=new FileWriter(writername);
				world.toStream(wwrite);
			}catch(IOException e){
				System.out.println("error");
			}
		}
	}

	public CmdLine(String[] args) {
		System.out.println("Ulisse the Robot - Laboratorio di software per le applicazioni - math.unifi.it (2012)");
		world=new World();
		//inizializzo i e lancio un ciclo while: l'utente scrive pincopallo
		//mentre i<numero di argomenti che ho inserito (qui è 1) svolgo il ciclo
		int i=0;
		while(i<args.length) {
			//inizializzo un'altra variabile arg, di tipo stringa, nella quale ho salvato 
			//la prima parola che l'utente ha scritto sul terminale
			String arg=args[i];
			i++;
			//a seconda di cosa ha scritto entro in una specifica parte del ciclo if
			//per confrontarlo uso il comando equals
			if (arg.equals("-help")) {
				help();

				//indico quale "funzione" lanciare , cosa fa la funzione lo specifico dopo
			} else if (arg.equals("-world")){

				//implemento per poter leggere la parola successiva scritta dall'utente

				//definisco una variabile filename, che pongo uguale 
				//a questa parola successiva
				String filename=args[i];
				i++;
				readWorld(filename);

			}
			else if (arg.equals("-program")){

				//dichiaro una variabile programName di tipo String

				String programName=args[i];
				i++;
				readProgram(programName);

			}


			else if (arg.equals("-run")) {

				runGame();
			}

			else if (arg.equals("-write")){

				String writername=args[i];
				createWrite(writername);

			}
			else if (arg.equals("-print")){
				print();

			}
			else if (arg.equals("-shell")){
				createShell();
				break;

			}else if (arg.equals("-gui")){
				i++;
				GuiApplication.launch();
			}
			else {
				System.out.println("unkown command");
				help();
				break;

			}
		}

	}


	/**
	 * start application
	 */
	public void launch() {
		GuiApplication.launch();
		// not yet implemented
	}
	public void help() {
		System.out.println("-program somefile.rbt: load the Program from the specified file");
		System.out.println("-world somefile.wrl: load the World description from the specified file");
		System.out.println("-run: run the given Program on the given World");
		System.out.println("-print: prints world description on System.out");
		System.out.println("-write somefile.wrl: save the current world to the file");
		System.out.println("-shell: run the interactive shell");
		System.out.println("-gui: start the graphical user interface");
		System.out.println("-help: print brief instructions");
	}

}
