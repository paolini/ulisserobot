import java.io.IOException;
import java.io.LineNumberReader;
import java.io.Reader;

/**
 * This class implements a tokenization of an input stream.
 * Tokens can be:
 *   IDENTIFIERS: a sequence of letters, digit and underscore character beginning with a non digit
 *   INTEGERS: a sequence of digits
 *   SEPARATORS: printable single characters: punctuation, parenthesis...
 *   STRINGS: any sequence of characters enclosed in double quotes: "..."
 *   
 * Rules:
 *   - spaces are used to separate IDENTIFIERS and INTEGERS but spaces are otherwise ignored
 *   - newlines and tabs are treated like spaces
 *   - the character # and anything following it on a line is considered a comment and is
 *     treated like a space
 *     
 * Functionality:
 *   - query next token: tokenIsAvailable, tokenIsIdentifier, tokenIsInteger, tokenIsSeparator, tokenIsString
 *   - get token: getIdentifier, getInteger, getSeparator, getString
 *   - skip expected token: expectIdentifier, expectSeparator, expectEndOfFile
 *   - skip any token: skipToken
 * 
 * Closeable:
 *   - close: close the underlying stream
 */
public class Tokenizer extends LineNumberReader {
	/**
	 * create a new Tokenizer which will read tokens from input stream
	 * @param in input stream
	 */
	public Tokenizer(Reader in) throws ParseError{
		super(in);
		tokenType = TokenType.IDENTIFIER; // dummy
		charPending = -1;
		nextToken();
	}
	
	/** 
	 * check for the availability of some token.
	 * @return false if there are no more tokens available
	 */
	public boolean tokenIsAvailable() {
		return tokenType != TokenType.ERROR && tokenType != TokenType.EOF;
	}
	
	/** 
	 * @return true if a token is available and it is an identifier
	 */
	public boolean tokenIsIdentifier() {
		return tokenType == TokenType.IDENTIFIER;
	}
	
	/**
	 * @return true if a token is available and it is an integer
	 */
	public boolean tokenIsInteger() {
		return tokenType == TokenType.INTEGER;
	}
	
	/**
	 * @return true if a token is available and it is a separator
	 */
	public boolean tokenIsSeparator() {
		return tokenType == TokenType.SEPARATOR;
	}
	
	/**
	 * @return true if a token is available and it is a string
	 */
	public boolean tokenIsString() {
		return tokenType == TokenType.STRING;
	}

	/**
	 * return the next token which is assumed to be an identifier
	 * @return a string representing the identifier
	 * @throws ParseError if the next token is not an identifier
	 */
	String getIdentifier() throws ParseError {
		if (tokenType != TokenType.IDENTIFIER)
			throw new ParseError("Identifier expected",getLineNumber());
		String r = stringToken;
		nextToken();
		return r;
	}
	
	/**
	 * return the next token which is assumed to be an integer
	 * @return the integer corresponding to the token
	 * @throws ParseError if next token is not a valid integer
	 */
	int getInteger() throws ParseError {
		if (tokenType != TokenType.INTEGER)
			throw new ParseError("Integer expected",getLineNumber());
		int r = intToken;
		nextToken();
		return r;
	}
	
	/**
	 * return the next token which is assumed to be a separator
	 * @return a character representing the separator
	 * @throws ParseError if the next token is not a separator
	 */
	char getSeparator() throws ParseError {
		if (tokenType != TokenType.SEPARATOR)
			throw new ParseError("Separator expected",getLineNumber());
		char r = charToken;
		nextToken();
		return r;
	}
	
	/**
	 * return the next token which is assumed to be a string enclosed in quotes
	 * @return the String (quotes excluded)
	 * @throws ParseError if the next token is not a valid string
	 */
	String getString() throws ParseError {
		if (tokenType != TokenType.STRING)
			throw new ParseError("String excpected",getLineNumber());
		String r = stringToken;
		nextToken();
		return r;
	}

	/**
	 * assumes the next token is a specified identifier
	 * @param identifier the identifier we expect
	 * @throws ParseError if next token is not what expected
	 */
	void expectIdentifier(String identifier) throws ParseError {
		String s = getIdentifier();
		if (!identifier.equals(s))
			throw new ParseError("Identifier '"+identifier+"' expected, '"+s+"' found",getLineNumber());
	}
	
	/**
	 * assumes the next token is a specified separator
	 * @param separator the separator character expected
	 * @throws ParseError if next character is not what expected
	 */
	void expectSeparator(char separator) throws ParseError {
		char c = getSeparator();
		if (c!=separator)
			throw new ParseError("Separator '"+separator+"' expected, '"+c+"' found");
	}
	
	/** assumes that the stream is finished
	 * @throws ParseError if there is some token remaining
	 */
	void expectEndOfFile() throws ParseError {
		if (tokenType != TokenType.EOF)
			throw new ParseError("End of file expected",getLineNumber());
		nextToken();
	}
	
	/**
	 * skip to next token.
	 * @throws ParseError if the stream was finished
	 */
	void nextToken() throws ParseError {
		if (tokenType == TokenType.EOF)
			return;
		if (tokenType == TokenType.ERROR)
			return;

		while(true) {
			int x = getChar();
			if (x<0) {
				tokenType = TokenType.EOF;
				return;
			}
			char c = (char) x;
			if (Character.isWhitespace(c)) {
				continue; // not really needed
			} else if (c=='#') {
				do {
					x = getChar();
					if (x<0) break;
					c = (char) x;
				} while(c != '\n');
				continue; // not really needed
			} else if (Character.isDigit(c) || c=='-') {
				intToken = 0;
				int sign = 1;
				if (c=='-') {
					sign = -1;
					c=(char)getChar();
					if (!Character.isDigit(c)) {
						throw new ParseError("digit expected after - sign",getLineNumber());
					}
				}
					
				do {
					intToken = intToken * 10 + Character.digit(c,10);
					x = getChar();
					if (x<0) break;
					c = (char) x;
				} while(Character.isDigit(c));
				intToken *= sign;
				ungetChar(x);
				tokenType = TokenType.INTEGER;
				return;
			} else if (Character.isLetter(c) || c == '_') {
				stringToken = "";
				do {
					stringToken += c;
					x = getChar();
					if (x<0) break;
					c = (char) x;
				} while(Character.isLetterOrDigit(c) || c == '_');
				ungetChar(x);
				tokenType = TokenType.IDENTIFIER;
				return;
			} else if (c=='"') {
				stringToken = "";
				x = getChar();
				c = (char) x;
				while(c!='"') {
					stringToken += c;
					x = getChar();
					if (x<0) throw new ParseError("end of file while parsing string",getLineNumber());
					c = (char) x;
				}
				tokenType = TokenType.STRING;
				return;
			} else {
				charToken = c;
				tokenType = TokenType.SEPARATOR;
			    return;
			}
 		}
	}
	
	private int getChar() throws ParseError {
		if (charPending >= 0) {
			int r = charPending;
			charPending = -1;
			return r;
		}
		try {
   		  return read();
		} catch (IOException e) {
			throw new ParseError("IOException");
		}
	}
	
	private void ungetChar(int c) {
		if (charPending >= 0) 
			throw new RuntimeException();
		charPending = c;
	}

	public enum TokenType {
		IDENTIFIER,
		INTEGER,
		SEPARATOR,
		STRING,
		EOF,
		ERROR
	}

	private TokenType tokenType;
	private String stringToken;
	private int intToken;
	private char charToken;
	private int charPending; 
}
