import java.io.FileNotFoundException;
import java.io.Reader;
import java.io.FileReader;

/**
 * The WorldReader class is used to read a World description from a file
 * (or more generally from an input stream)
 */

public class WorldReader extends Tokenizer {
	/**
	 * creates a WorldReader connected to the given input stream
	 * @param in input stream whence the world description is read
	 * @throws ParseError 
	 */
	public WorldReader(Reader in) throws ParseError {
		super(in);	
	}
	
	/**
	 * create a WorldReader and prepare reading from the file specified
	 * @param filename the name of the file whence the description is read from
	 * @throws FileNotFoundException 
	 * @throws ParseError 
	 */
	public WorldReader(String filename) throws FileNotFoundException, ParseError {
		super(new FileReader(filename));		
	}
	
	/**
	 * reads the World description from the input stream and
	 * creates a corresponding World object. 
	 * After calling this method the WorldReader must be closed
	 * @return the World object described in the input stream
	 * @throws ParseError if there are errors in the input stream
	 */
	void readWorld(World world) throws ParseError {     
	expectIdentifier("Width");
	expectSeparator('=');
	int width=getInteger();
	expectIdentifier("Height");
	expectSeparator('=');
	int height=getInteger();
	world.resize(width,height);
	expectIdentifier("Ulisse_x");
	expectSeparator('=');
	int ulisseX=getInteger();           
	expectIdentifier("Ulisse_y");
	expectSeparator('=');
	int ulisseY=getInteger();
	world.moveUlisseTo(ulisseX, ulisseY);
	expectIdentifier("Diamonds");
	expectSeparator('=');
	int diamonds=getInteger();
	world.setUlisseBag(diamonds);
	expectIdentifier("Orientation");
	expectSeparator('=');
	int orientation=getInteger();
	world.setUlisseOrientation(orientation);                                             
	for (int j=0;j<world.getHeight();j++){
		for (int i=0; i< world.getWidth(); i++){
			expectSeparator('(');
			int d=getInteger();
			if (d!=i)
				throw new ParseError("invalid number: "+j+" expected, "+d+" found",getLineNumber());
			expectSeparator(',');
			int e=getInteger();
			if (e!=j)
				throw new ParseError("invalid number: "+i+" expected, "+e+" found",getLineNumber());
			expectSeparator(')');
			expectSeparator('=');
			expectSeparator('(');
			int n=getInteger();
			world.setDiamondsInCell(i, j, n);
			expectSeparator(',');
			String h=getIdentifier();
			if (h.equals("true"))
				world.setHorizontalWall(i,j,true);
			else if (h.equals("false"))
				world.setHorizontalWall(i,j,false);
			else 
				throw new ParseError("invalid string",getLineNumber());
			expectSeparator(',');
			String v=getIdentifier();
			if (v.equals("true"))
				world.setVerticalWall(i,j,true);
			else if (v.equals("false"))
				world.setVerticalWall(i,j,false);
			else 
				throw new ParseError("invalid string",getLineNumber());
			
			expectSeparator(')');
		}
	}
	
}
}