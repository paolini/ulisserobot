import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;

/**
 * This class contains the code to draw the world on the canvas.
 */
public class GuiCanvas extends Canvas {
	private GuiApplication guiApplication;
	int widthcell;
	
	public GuiCanvas(GuiApplication x) {
		guiApplication=x;
	}

	/**
	 * This method gets automatically called whenever the canvas needs to be redrawn.
	 */
	public void paint(Graphics g) {
		
		World world = guiApplication.getWorld();
		int x=world.getWidth();
		int y=world.getHeight();    
		int i,j,jj;
	
		/**
		 * Cells' dimension.
		 */
		widthcell=30;
		
		/**
		 * Grid's construction.
		 */
		for(i=0; i<=x; i++) {			
			g.drawLine(i*widthcell, 0, i*widthcell, y*widthcell);
			}
        for(jj=0; jj<=y; jj++) {
        	j=y-jj;
        	g.drawLine(0, j*widthcell, x*widthcell, j*widthcell);
        	}
        /**
    	 * Representation of Ulisse basing on the World's parameters.
    	 */
        g.setColor(Color.magenta);
        int x1=world.getUlisseX();
        int y1=y-1-world.getUlisseY();
        int o=world.getUlisseOrientation();        
        /**
    	 * To represent Ulisse we use a circle with an arrow oriented using:
    	 * 0 = east, 1 = north, 2 = west, 3 = south.
    	 */
        switch (o) {
        	case 0: {
        		g.drawArc(x1*widthcell, y1*widthcell, widthcell, widthcell, 0, 360);
        		g.drawLine(x1*widthcell, (y1)*widthcell+widthcell/2, (x1+1)*widthcell, y1*widthcell+widthcell/2);
        		g.drawLine(x1*widthcell+widthcell/2, y1*widthcell, (x1+1)*widthcell, y1*widthcell+widthcell/2);
        		g.drawLine(x1*widthcell+widthcell/2, (y1+1)*widthcell, (x1+1)*widthcell, y1*widthcell+widthcell/2);
        	}
        	break;
        
        	case 1: {
        		g.drawArc(x1*widthcell, y1*widthcell, widthcell, widthcell, 0, 360);
        		g.drawLine(x1*widthcell+widthcell/2, y1*widthcell, x1*widthcell+widthcell/2, (y1+1)*widthcell);
        		g.drawLine(x1*widthcell+widthcell/2, y1*widthcell, x1*widthcell, y1*widthcell+widthcell/2);
        		g.drawLine(x1*widthcell+widthcell/2, y1*widthcell, (x1+1)*widthcell, y1*widthcell+widthcell/2);
        	}
        	break;
        
        	case 2: {
        		g.drawArc(x1*widthcell, y1*widthcell, widthcell, widthcell, 0, 360);
        		g.drawLine(x1*widthcell, y1*widthcell+widthcell/2, (x1+1)*widthcell, y1*widthcell+widthcell/2);
        		g.drawLine(x1*widthcell, y1*widthcell+widthcell/2, x1*widthcell+widthcell/2, y1*widthcell);
        		g.drawLine(x1*widthcell, y1*widthcell+widthcell/2, x1*widthcell+widthcell/2, (y1+1)*widthcell);
        	}
        	break;
        
        	case 3: {
        		g.drawArc(x1*widthcell, y1*widthcell, widthcell, widthcell, 0, 360);
        		g.drawLine(x1*widthcell+widthcell/2, (y1+1)*widthcell, x1*widthcell+widthcell/2, y1*widthcell);
        		g.drawLine(x1*widthcell+widthcell/2, (y1+1)*widthcell, x1*widthcell, y1*widthcell+widthcell/2);
        		g.drawLine(x1*widthcell+widthcell/2, (y1+1)*widthcell, (x1+1)*widthcell, y1*widthcell+widthcell/2);
        	}
        	break;
        }
        
        /**
    	 * We use World's parameters to represent the diamonds and walls.  
    	 */
		for(i=0; i<x; i++) {
			for(jj=0; jj<y; jj++) {
				j=y-1-jj;
	    	  	int ndiamonds = world.getNDiamondsInCell(i,jj);
	    	  	if (ndiamonds!=0 ){
	        		g.setColor(Color.blue);
	        		g.drawLine(i*widthcell+widthcell/2, j*widthcell, (i+1)*widthcell, j*widthcell+widthcell/2);
	        		g.drawLine(i*widthcell+widthcell/2, (j+1)*widthcell, (i+1)*widthcell, j*widthcell+widthcell/2);
	        		g.drawLine(i*widthcell+widthcell/2, j*widthcell, i*widthcell, j*widthcell+widthcell/2);
	        		g.drawLine(i*widthcell+widthcell/2, (j+1)*widthcell, i*widthcell, j*widthcell+widthcell/2);    			
	        		if(ndiamonds<10){
	        			g.drawString("" + ndiamonds, i*widthcell+2*widthcell/5, j*widthcell+2*widthcell/3);
	        		}
	        		else
	        			if(ndiamonds<100){
	        				g.drawString("" + ndiamonds, i*widthcell+widthcell/4, j*widthcell+2*widthcell/3);	
	        			}
				}
	    	  	if(world.isHorizontalWall(i, jj)==true && j<=y-1 && j>0){					        
	        		for(int k=1;k<=widthcell/5;k=k+2){
        				g.setColor(Color.red);
        				g.drawLine(i*widthcell+5*k, j*widthcell-5+widthcell/10, i*widthcell+5*(k+1), j*widthcell+widthcell/10);
        			}
        			for(int k=1;k<=widthcell/5;k=k+2){
        				g.setColor(Color.red);
        				g.drawLine(i*widthcell+5*k, j*widthcell-5+widthcell/10, i*widthcell+5*(k-1), j*widthcell+widthcell/10);
        			}
	        	}
	        	if(world.isVerticalWall(i, jj)==true && i<x-1){	
	        		for(int k=1;k<=widthcell/5;k=k+2){
	    	        	g.setColor(Color.red);
	        			g.drawLine((i+1)*widthcell-5+widthcell/10, j*widthcell+5*k, (i+1)*widthcell+widthcell/10, j*widthcell+5*(k-1));
	        		}
	        		for(int k=1;k<=widthcell/5;k=k+2){
	    	        	g.setColor(Color.red);
	        			g.drawLine((i+1)*widthcell-5+widthcell/10, j*widthcell+5*k, (i+1)*widthcell+widthcell/10, j*widthcell+5*(k+1));
	        		}
	        	}
			}
		}
	}
	/**
	 * @return width of every cell.
	 */
	public int getwidthcell(){
		 return widthcell;
	}
}