/**
 * application entry point
 */
public class UlisseRobot {
	public static void main(String[] args) {
		CmdLine cmdLine = new CmdLine(args);
		if (args.length==0){
			cmdLine.launch();
		}
	}
}
