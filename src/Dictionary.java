/*This class implements a list composed by 2 columns: names of functions (Strings) in the first one;
 * shorts in the second one.
 * it contains also the methods of storage and lecture of the functions.
 */
  
public class Dictionary {
	/**
	 * 256 is the default length of the two arrays that implement the dictionary
	 */
	
	private static final int maxLength=256;
	           
	/**
	 * dictionary constructor: 'Keys' is the function array
	 *                         'Values' is the shorts array
	 * Length is the number of stored functions 
	 */
	public Dictionary () {
		Keys = new String [maxLength];
		Values = new short [maxLength];
		Length = 0;	
	}
			
	/**
	 * stores the function and its value in the dictionary
	 * @param Key: name of the function
	 * @param Value: number representing functions
	 * @throws ParseError if the function is yet stored or the dictionary is full
	 */
	public void store(String Key, short Value ) throws ParseError{
		if (control(Key)!=-1){
			throw new ParseError("function yet stored");
		}
    	Keys [Length] = Key;
		Values [Length] = Value;
		Length = Length + 1 ;
		if (Length>=maxLength) throw new ParseError("too many function in Dictionary");
	}
			
	/**
	 * return the value of the stored function
	 * @param fun: the name of the searched function
	 * @return the function value
	 * @throws ParseError: if the function is invalid (not stored)
	 */
	public short  get(String fun) throws ParseError{
		int y= control(fun);
		if(y==-1){
			throw new ParseError("function "+ fun + " not stored");
		}
		short x= Values [y];
		return x;
	}
	
	
	
	private int Length;
	private String Keys [];
	private short Values[];
	
	/** 
	 * this function "reads" the dictionary and control if there is a function and its location
	 * @param name: the name of the function
	 * @return the address of the function, returns -1 if the function is not present
	 */
	public int control(String name){
		for (int i=0; i<Length; i++){
			if (Keys[i].equals(name)){
				return i;
				}
		}
		return -1;
	}

}

