/**
 * This class implements a buffer (i.e. a list) of short integers.
 * The buffer automatically adapts its size to contain new elements
 */
public class BufferOfShorts {
	/**
	 * constructs a new empty buffer
	 * 10 is the default length
	 */
	public BufferOfShorts() {
		buffer= new short [10];
		currentLength = 0;
	}
	
	/**
	 * @return the length (number of shorts) of the buffer
	 */
	public int reservedLength() {
		return buffer.length;
	}
	
	/**
	 * @return the length of the number of shorts used by the buffer 
	 */
	public int length() {
		return currentLength; 
	}
	/**
	 * add the given value to the end of the buffer, increasing its size by one
	 * @param value: the value being added
	 */
	public void add(short value) {
		if (currentLength==buffer.length){
			growBuffer();
		}
		buffer[currentLength]=value;
		currentLength++;
	}
	
	/**
	 * removes the last element from the buffer
	 * @return the element removed
	 */
	public short pop() {
		if (currentLength == 0)
			throw new RuntimeException("Invalid buffer access");
		currentLength--;
		return buffer[currentLength];
	}
	
	/**
	 * return the value stored in position @param offset
	 * @param offset: the position of the element being retrieved (0 is the first)
	 * @return the value stored at the given position
	 * @throws RuntimeException: if the offset is invalid (negative or larger than length)
	 */
	public short get(short offset) {
		if (offset<0 || offset >= currentLength){
			throw new RuntimeException("invalid buffer access");
		}
		short x=buffer[offset];
		return x; 
	}
	
	/**
	 * sets the value of the integer stored at position offset
	 * @param offset: position of the value to be changed
	 * @param value: new value to be stored in position
	 * @throws RuntimeException if the offset is invalid (negative or larger than length)
	 */
	public void set(short offset, short value) {
		if (offset<0 || offset >= currentLength){
			throw new RuntimeException("invalid buffer access");
		}
		buffer[offset]= value;
		
	}
 
	private short buffer [];
	private int currentLength;
	
	/**
	 * this function doubles the length of the buffer 
	 */
	private void growBuffer () {
		short newBuffer [];
		newBuffer=new short [reservedLength()*2];
		for (int i=0; i<currentLength; i++){
			newBuffer[i]= buffer[i];
		}
		buffer=newBuffer;
	}
}
