                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      /**
 * 
 * A Program is a compiled sequence of instructions
 * 
 * Instructions might by composed by one, two or three (short) integers
 * First integer is always a command code.
 * 
 * Instructions:
 * 
 * 0 ERROR: put the virtual machine into error state
 * 
 * 1 BUILTIN code: calls a primitive method of World: "advance", "turnLeft", "isFrontClear",...
 *   the instruction called is identified with an integer code [2 integers]
 *   
 * 2 CALL address: makes a function call to the specified address [2 integers]
 *    
 * 3 RETURN: return to the caller or ends program [1 integer]
 * 
 * 4 JUMP address: makes a jump to the specified address
 * 
 * 5 JUMPTRUE address: conditional jump to the specified address 
 *   if the virtual machine register is in state true [2 integers]
 * 
 * 6 JUMPFALSE address: conditional jump to the specified address 
 *   if the virtual machine register is in state false [2 integers]
 * 
 * IMPLEMENTATION: uses a BufferOfShorts to manage the resizing of the program
 */

public class Program extends BufferOfShorts {
	
	/**
	 * Gives the length of the program which is eventually the address of the 
	 * next added instruction
	 * @return the number of integers comprising the current program
	 */
	public short addr() {
		return (short) length();
	}
	
	/**
	 * Adds an ERROR instruction to the program
	 */
	public void addErrorInstruction() {
		add((short) 0);
	}
	
	/**
	 * Adds a BUILTIN instruction to the program
	 * @param code: the code of the builtin instruction
	 */
	public void addBuiltin(short code) {
		add ((short) 1);
		add ((short) code);
	}
	
	/**
	 * Adds a CALL instruction
	 * @param address: the address of the function being called
	 * @return the address of the address useful when the address should be changed later
	 */
	public short addCall(short address) {
		add ((short) 2); 
		add ((short) address);
		return (short) (addr()-1); 
	}

	/**
	 * Adds a RETURN instruction
	 */
	public void addReturn() {
		add ((short) 3);		
	}
	
	/**
	 * Adds a JUMP instruction
	 * @param address: the target address of the JUMP
	 * @return the address of the address (see CALL)
	 */
	public short addJump(short address) {
		add ((short) 4);
		add ((short) address);
		return (short) (addr()-1);
	}
	
	/**
	 * Adds a JUMPTRUE instruction
	 * @param address: the target address of the JUMPTRUE
	 * @return the address of the address (see CALL)
	 */
	public short addJumpTrue(short address) {
		add ((short) 5);
		add ((short) address);
		return (short) (addr()-1);
	}

	/**
	 * Adds a JUMPFALSE instruction
	 * @param address: the target address of the JUMPFALSE
	 * @return the address of the address (see CALL)
	 */
	public short addJumpFalse(short address) {
		add ( (short) 6);
		add ((short)address);
		return (short) (addr()-1);
	}
}
