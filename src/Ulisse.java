
/**
 * This class contains the methods of Ulisse
 */
public class Ulisse {
	private int bag;
	private int x;
	private int y;
	private int orientation;
	private boolean died;
	
	
	Ulisse(){
		bag=-1;
	}
	/**
	 * @return number of diamonds in Ulisse's bag
	 */
	int getBag() {
		return bag;
	}
	/**
	 * @param n is the updated number of diamonds in Ulisse's bag
	 */
	void setBag(int n) {
		bag=n;
	}
	/**
	 * @return coordinate x of Ulisse's position
	 */
	int getX() {
		return x;
	}
	/**
	 * @param xnew is the updated coordinate x
	 */
	void setX(int xnew) {
		x=xnew;
	}
	/**
	 * @return coordinate y of Ulisse's position
	 */
	int getY() {
		return y;
	}
	/**
	 * @param ynew is the updated coordinate y
	 */
	void setY(int ynew) {
		y=ynew;
	}
	/**
	 * @return Ulisse's orientation
	 */
	int getOrientation() {
		return orientation;
	}
	
	/**
	 * Rotates Ulisse changing his orientation
	 * @param orientation 0=EAST, 1=NORTH, 2=WEST, 3=SOUTH
	 */
	
	void setOrientation(int o) {
		orientation=o;
	}
	
	/**
	 * @return true if Ulisse is died
	 *         false otherwise
	 */
	boolean getDied() {
		return died;
	}
	/**
	 * @param flag=true if Ulisse hits a wall, or if he attempts to take a diamond from an empty cell
	 *             false if Ulisse is alive
	 */
	void setDied(boolean flag) {
		died=flag;
	}
}
