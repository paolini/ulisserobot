import java.util.Stack;
/**
 * a "stack" of addresses (i.e. short integers)
 * uses a BufferOfShorts to manage the resizing of the stack
 */



public class VirtualMachineCallStack extends BufferOfShorts {
	

	/**
	 * creates a new, empty stack
	 */
	
	public VirtualMachineCallStack() {
	}
	
	/**
	 * is the stack empty?
	 * @return true if the stack is empty
	 */
	
	public boolean isEmpty() {
		return length()==0;	
	}
	
	/**
	 * adds an address to the top of the stack
	 * @param address: the address being added
	 */
	public void push(short address) {
		add(address);	
		}
			
}
