import java.io.File;

import javax.swing.filechooser.*;

class WrdFileFilter extends FileFilter{

	public boolean accept(File file){
		if(file.isDirectory())
			return true;
		
		String NameFile;
		NameFile=file.getName().toLowerCase();
		return NameFile.endsWith("wrd") || NameFile.endsWith("prg");
	}

	public String getDescription(){
		return "File World or File Program";
	}
}
