/**
 * This class represents an exception raised by the VirtualMachine
 * this usually corresponds to an invalid instruction, and hence should
 * only happen when the compiler does not work correctly
 */

public class VirtualMachineError extends Exception {
		
	/**
	 * creates a new VirtualMachineError with specified error message
	 * @param message: a description of the error occurred
	 */
	
	private String error;
	
	public VirtualMachineError(String message) {
		// not yet implemented
		error=message;
	}

	public String toString() {
		return error;
	}
	
	/**
	 * suggested by eclipse
	 */
	private static final long serialVersionUID = 1L;
}
