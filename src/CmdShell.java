/**
 * This class implements a shell (textual interaction with User)
 * 
 * It should present a PROMPT to the user and read input one line at a time.
 * 
 * General commands:
 * 
 * program "somefile.rbt": load Program from specified file
 * world "somefile.wrl": load World from specified file
 * run: run the Program
 * step: execute one step of the program
 * print: show World on screen
 * 
 * Ulisse Commands:
 * 
 * advance, turnLeft, isFrontClear,...: execute command on given world
 * 
 * Define new commands and add them to the Program:
 *  
 * newFunction = { .... }
 * 
 * 
 */

public class CmdShell {
	public CmdShell() {
		
		// not yet implemented
	}
}
