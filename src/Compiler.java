import java.io.Reader;


/**
 * The compiler is an object capable of taking a source code in Ulisse Program language
 * and construct a Program which can be executed on the VirtualMachine
 * <P>
 * EXAMPLE OF SOURCE CODE
 * <PRE>
 * # this program makes Ulisse 
 * # clean all diamonds in an empty room
 *
 * take_all_diamonds = {
 *   while diamond_is_present {
 *     take_diamond
 *   }
 * } 
 * 
 * clean_to_wall = {
 *   take_all_diamonds
 *   while front_is_clear {
 *     advance
 *     take_all_diamonds
 *   }
 * }
 * 
 * advance_to_wall = {
 *   while front_is_clear {
 *     advance
 *   }
 * }
 * 
 * main = {
 *   advance_to_wall
 *   turn_left
 *   advance_to_wall
 *   turn_left
 *   
 *   # here Ulisse is in a corner, with a wall on right-hand side
 *   
 *   while true {
 *     clean_to_wall
 *     turn_left
 *     if not front_is_clear {
 *        return # mission accomplished!
 *     }
 *     advance
 *     turn_left
 *     clean_to_wall
 *     turn_right
 *     if not front_is_clear {
 *        return # mission accomplished!
 *     }
 *     turn_right
 *   }
 * }
 * </PRE>
 * <p>
 * FORMAL DESCRIPTION OF THE SOURCE CODE:
 * See http://en.wikipedia.org/wiki/Extended_Backus�Naur_Form
 * <PRE>
 * Program = { Function } ;
 * Function = Identifier, "=", Block ;
 * Identifier = { alphanumeric characters or underscores or hyphens }
 * Block = "{",  { Instruction },  "}" ;
 * Instruction = IfStatement | WhileStatement | ReturnStatement | BreakStatement | ContinueStatement | FunctionCall ;
 * FunctionCall = BuiltinFunction | Identifier ;
 * BuiltinFunction = "advance" | "turnLeft" | "turnRight" | "front_is_clear" | "left_is_clear" | "right_is_clear" |
 *                   "take_diamond" | "drop_diamond" | "diamond_is_present" | 
 *                   "facing_east" | "facing_north" | "facing_west" | "facing_south" |
 *                   "true" | "false"
 * BlockOrInstruction = Block | Instruction ;
 * IfStatement = "if", [ "not" ], FunctionCall, BlockOrInstruction, [ "else ", BlockOrInstruction ];
 * WhileStatement = "while", [ "not "], FunctionCall  , BlockOrInstruction;
 * ReturnStatement = "return", [ FunctionCall ] ;
 * BreakStatement = "break" ;
 * ContinueStatement = "continue" ;
 * </PRE>
 */

public class Compiler {
	/**
	 * Compiles the source code provided in input.
	 * @return the compiled code as a Program object
	 * @throws ParseError if there are errors in source code
	 */
	Tokenizer tokenizer;
	Dictionary dictionary;
	Program program;
	short mainAddr; // address of "main" function
	
	public Program compile(Reader input) throws ParseError {
	       tokenizer = new Tokenizer(input);
	       program = new Program();
	       dictionary=new Dictionary();
	       mainAddr = program.addJump((short)0);
		while(tokenizer.tokenIsAvailable())
			compileFunction();
			
		return program;
	}
	/* PRIVATE IMPLEMENTATION */
	
	/**
	 * Internally the compiler must keep a dictionary with already compiled function
	 * (see TreeMap class to implement the dictionary)
	 */

	/**
	 * this function stores the entry point (first address) of a compiled function
	 * @param name: name of the function
	 * @param address: first instruction of the compiled function
	 * @throws ParseError if the name is already present in the dictionary
	 */
	private void storeFunction(String name, short entryPoint) throws ParseError {
		dictionary.store(name, entryPoint);
	}
	
	/**
	 * searches the function with given name in the dictionary of compiled functions
	 * @param name: the name of the function
	 * @return the entry point of the compiled function
	 * @throws ParseError if the name is not present in the dictionary
	 */
	private short getFunction(String name) throws ParseError {
		return dictionary.get(name); 
	}
	
	/**
	 * reads a function from the input stream,
	 * compiles it and append it to the current program
	 * stores the address of the function in the dictionary
	 * @throws ParseError if the source code is not valid
	 */
	private void compileFunction() throws ParseError {
		String namefunction;
		namefunction=tokenizer.getIdentifier();
		short addr=program.addr();
		if (namefunction.equals("main")) {
			program.set(mainAddr, addr); // initial jump to main function
		}
		storeFunction(namefunction, addr);
        tokenizer.expectSeparator('=');
		compileBlock((short)-1,(short)-1);
		program.addReturn();
	}
	
	/**
	 * reads a block of instructions delimited by braces
	 * compiles it appending to the current Program
	 * @param breakAddr: where to jump when a "break" statement is found
	 *        if negative a break is not allowed in this block
	 * @param continueAddr: where to jump when a "continue" statement is found,
	 *        if negative a continue is not allowed in this block
	 * @throws ParseError if an error is found in the source code
	 */
	private void compileBlock(short breakAddr, short continueAddr) throws ParseError {
		if(tokenizer.tokenIsSeparator()){
			tokenizer.expectSeparator('{');
			while(tokenizer.tokenIsIdentifier()) {
				compileInstruction(breakAddr, continueAddr);
			}
			tokenizer.expectSeparator('}');
		} else {
			compileInstruction(breakAddr, continueAddr);
		
		}
	}

	/**
	 * reads a single instruction.
	 * compiles it appending the code to the current Program.
	 * parameters and throws as in compileBlock
	 * The instruction can be:
	 * - a builtin instruction ("advance", "turnLeft", "isFrontClear"...)
	 * - a function call (user defined function)
	 * - an "if" statement (with optional "else") use compileIfStatement
	 * - a "while" statement: use compileWhileStatement
	 * - a "return" statement
	 * - a "break" statement
	 * - a "continue" statement
	 */
	static String names[]={"","advance","turnLeft","turnRight","front_is_clear","left_is_clear","right_is_clear","take_diamond","drop_diamond","diamond_is_present","facing_east","facing_north","facing_west","facing_south","true","false"};
	short i;
	private void compileInstruction(short breakAddr, short continueAddr) throws ParseError {
	             String instruction;
	             instruction=tokenizer.getIdentifier();
	             if (instruction.equals("if"))
	            	 compileIfStatement(breakAddr , continueAddr);
	             else if (instruction.equals("while"))
	            	 compileWhileStatement(breakAddr,continueAddr);
	             else if(instruction.equals("return"))
	            	 program.addReturn();
	             else if(instruction.equals("break")){
	            	 if (breakAddr==-1)
	            		 throw new ParseError("break non valido,l'indirizzo -1 non e' corretto");
	            	 else program.addJump(breakAddr);
	             }
	             else if(instruction.equals("continue")){
	            	 if(continueAddr==-1)
	            		 throw new ParseError("continue non valido, l'indirizzo -1 non e' corretto");
	            	 else program.addJump(continueAddr);
	             }
	            	 
	             else {
	            	 for (i=1;i<names.length; i++) {
	            	    if (instruction.equals(names[i])) {
	            	    	program.addBuiltin(i);
	            	    	break;
	            	    }
	            	 }
	            	 if(i==names.length){
	            		 short addr=getFunction(instruction);
	            		 program.addCall(addr);
	            		 
	            		 
	            	 }
	             }		 
	}

	
	private void compileIfStatement(short breakAddr, short continueAddr) throws ParseError {
		String condition;
		String instruction;
		boolean negate;
		int code;
		short addr;
		

		condition=tokenizer.getIdentifier();
		if (condition.equals("not")){
			negate=true;
		    instruction=tokenizer.getIdentifier();
		   }
		
		else {
			negate=false;
			instruction=condition;
			}
		
		 for (i=1;i<names.length; i++) {
     	    if (instruction.equals(names[i])) {
     	    	program.addBuiltin(i);
     	    	break;
     	    }
     	 }
        if (negate)
        	addr=program.addJumpTrue((short)0);
        else 
        	addr=program.addJumpFalse((short)0);//a priori non conosco l'indirizzo della cella di memoria in cui si trova l'istruzione da eseguire in caso di ucita dal ciclo, indirizzo che comunque serve in input. Ne viene assegnato dunque uno fittizio.
        	
     	compileBlock(breakAddr,continueAddr);
		program.set(addr,program.addr());
	}
	
	private void compileWhileStatement(short breakAddr, short continueAddr) throws ParseError {
		String condition;
		String instruction;
		boolean negate;
		int code;
		short addr;
		
		continueAddr = program.addr();
		condition=tokenizer.getIdentifier();
		if (condition.equals("not")){
			negate=true;
		    instruction=tokenizer.getIdentifier();
		   }
		
		    else {
			      negate=false;
			      instruction=condition;
			      }
		
		 for (i=1;i<names.length; i++) {
     	    if (instruction.equals(names[i])) {
     	    	program.addBuiltin(i);
     	    	break;
     	    }
     	 }
		 if (i==names.length) 
			 throw new ParseError("not a valid builting instruction: "+instruction);
		 
		  if (negate)
	        	program.addJumpFalse((short)(program.addr()+4));
	        else 
	        	program.addJumpTrue((short)(program.addr()+4));//a priori non conosco l'indirizzo della cella di memoria in cui si trova l'istruzione da eseguire in caso di ucita dal ciclo, indirizzo che comunque serve in input. Ne viene assegnato dunque uno fittizio.
		   breakAddr = program.addr();
		   addr=program.addJump((short) 0);
		   compileBlock(breakAddr,continueAddr);
		   program.addJump(continueAddr);
		   program.set(addr,program.addr());
	}
}
