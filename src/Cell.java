/**
 * This class contains the methods of cells
 */
public class Cell  { 
	private int nDiamonds;
	private boolean hWall;
	private boolean vWall;
	
	/**
	 * @return the number of diamonds in cell
	 */
    public int getNDiamonds(){
        return nDiamonds;	
    }
   /**
    * @param n is the updated number of diamonds
    */
   public void setNDiamonds(int n){
	   nDiamonds=n;
   }
   /**
    * @return true if an horizontal wall is present 
    *         false otherwise
    */
   public boolean getHWall(){
	   return hWall;
   }
   /**
    * @param flag=true to add an horizontal wall in cell, 
    *             false to remove it
    */
   public void setHWall(boolean flag){
	   hWall=flag;
   }
   /**
    * @return true if an vertical wall is present 
    *         false otherwise
    */
   public boolean getVWall(){
	   return vWall;
   }
   /**
    * @param flag=true to add an vertical wall in cell, 
    *             false to remove it
    */
   public void setVWall(boolean flag){
	   vWall=flag;
   }
   
}
   
   


