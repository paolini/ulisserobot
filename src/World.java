import java.io.Reader;
import java.io.Writer;

/**
* This is the main class of the project. The World describes a rectangular room divided into width*height
* square cells. Width and height are positive integer numbers representing the number of cells
* in each side.
* Ulisse the Robot is in one of the cells. In every cell there might be one or more diamonds.
* Between a cell and the neighboring ones there can be walls, which Ulisse cannot cross.
* The perimeter of the room is always delimited by walls so that Ulisse cannot escape from the room.
* Ulisse has also a bag which contains diamonds. Initially the bag of diamonds can contain any number
* of diamonds (zero included) or may contain infinitely many diamonds.
* Ulisse has also an orientation which can be: facingEast, facingNorth, facingWest or facingSouth.
* Ulisse can be live or dead!
*
* The class has methods for loading from a stream and for saving to a stream: fromStream, toStream
*
* It has methods corresponding to the possible basic actions of Ulisse:
* ulisseAdvance, ulisseTurnLeft, ulisseTurnRight, ulisseTakeDiamond, ulisseDropDiamond,
* ulisseDiamondIsPresent, ulisseFrontIsClear, ulisseLeftIsClear, ulisseRightIsClear,
*
* It has some methods to modify the world:
* resize, moveUlisseTo, setUlisseOrientation, addDiamond, removeDiamond, setDiamonds, setHorizontalWall, setVerticalWall
*
* Also there are some methods to inspect the world:
* getWidth, getHeight, isHorizontalWall, isVerticalWall, getNDiamondsInCell
* getUlisseX, getUlisseY, getUlisseOrientation, getUlisseNDiamonds, isUlisseDied
*
*
*/
public class World {
/**
* Constructs a default World: an empty 10x10 room with Ulisse in the lower-left corner facing East.
* Ulisse has a bag with infinitely many diamonds.
*/
public World() {
ulisse=new Ulisse();
resize(10,10);
}

/**
* Writes a textual description of the World. Can be used to save World to a file.
* @param out destination stream (usually a file)
*/
public void toStream(Writer out) {
	WorldWriter writer=new WorldWriter(out);
	writer.write(this);
	writer.close();
}

/**
* Reads a textual description of the World. Can be used to load World from a file.
* @param in the stream where description is written (usually a file)
 * @throws ParseError 
*/
public void fromStream(Reader in) throws ParseError {
	WorldReader reader=new WorldReader(in);
	reader.readWorld(this);
}

/**
* A basic move of Ulisse: advance one square. If Ulisse hits a wall he dies!
* In this as in every Ulisse basic move, if Ulisse is died nothing happens.
*/
public void ulisseAdvance() {
 int x=ulisse.getX();
 int y=ulisse.getY();
    if (ulisseFrontIsClear()==true) {
       if (ulisseFacingEast()==true)
       ulisse.setX(x+1);
       else if (ulisseFacingNorth()==true)
       ulisse.setY(y+1);
       else if (ulisseFacingWest()==true)
       ulisse.setX(x-1);
       else if (ulisseFacingSouth()==true)
       ulisse.setY(y-1);
}
else
ulisse.setDied(true);
}

/**
* A basic move of Ulisse: rotate 90-degrees counter-clockwise
*/
public void ulisseTurnLeft() {
 int o=ulisse.getOrientation();
    if(ulisseFacingSouth()==true)
    ulisse.setOrientation(0);
    else
    ulisse.setOrientation(o+1);
}

/**
* A basic move of Ulisse: rotate 90-degrees clockwise
*/
public void ulisseTurnRight() {
 int o=ulisse.getOrientation();
    if(ulisseFacingEast()==true)
    ulisse.setOrientation(3);
    else
    ulisse.setOrientation(o-1);
}


/**
 * Adds a diamond in Ulisse's bag
 */
public void addDiamondInBag(){
	if (ulisse.getBag()!=-1)
	ulisse.setBag(ulisse.getBag()+1);
}

/**
 * Removes a diamond from Ulisse's bag
 */
public void removeDiamondFromBag(){
	if (ulisse.getBag()!=-1)
	ulisse.setBag(ulisse.getBag()-1);
	}
	
/**
* A basic move of Ulisse: takes a diamond and put it into bag.
* If there is no diamond Ulisse dies!
*/

public void ulisseTakeDiamond() {
int x=ulisse.getX();
int y=ulisse.getY();
if (!ulisseDiamondIsPresent())
ulisse.setDied(true);
else
	addDiamondInBag();
    removeDiamond(x,y);
}

/**
* A basic move of Ulisse: takes a diamond from the bag and leaves on the ground.
* If the bag is empty Ulisse dies!
*/
public void ulisseDropDiamond() {
 int x=ulisse.getX();
 int y=ulisse.getY();
    if (ulisse.getBag()==0)
    ulisse.setDied(true);
    else
    removeDiamondFromBag();
    addDiamond(x,y);
}

/**
* A basic move of Ulisse: is there a diamond in the current cell?
* @return true if a diamond is present
*/
public boolean ulisseDiamondIsPresent() {
 int x=ulisse.getX();
 int y=ulisse.getY();
 if (getNDiamondsInCell(x,y)==0 )
        return false;
    else
    	return true;
}




/**
* A basic move of Ulisse: is there a wall in front of Ulisse?
* @return true if there is no wall
*/
public boolean ulisseFrontIsClear() {
	int x=ulisse.getX();
	int y=ulisse.getY();

if(ulisseFacingEast()==true){  
return !isVerticalWall(x,y);   
}

if (ulisseFacingNorth()==true){
return !isHorizontalWall(x,y);
}
if (ulisseFacingWest()==true){
return !isVerticalWall(x-1,y);
}
if (ulisseFacingSouth()==true){
return !isHorizontalWall(x,y-1);
}
return false;
}



/**
* A basic move of Ulisse: is there a wall on the left-hand side of Ulisse?
* @return true if the is no wall
*/
public boolean ulisseLeftIsClear() { 
int x=ulisse.getX();
int y=ulisse.getY();
if (ulisseFacingSouth()==true){
	return !isVerticalWall(x,y);
}
if (ulisseFacingEast()==true){
	return !isHorizontalWall(x,y);
		}
if (ulisseFacingNorth()==true){
	return !isVerticalWall(x-1,y);
		}	
if (ulisseFacingWest()==true){
	return !isHorizontalWall(x,y-1);
		}	
return false;
}



/**
* A basic move of Ulisse: is there a wall on the right-hand side of Ulisse?
* @return true if the is no wall
*/
public boolean ulisseRightIsClear() { 
int x=ulisse.getX();
int y=ulisse.getY();
if (ulisseFacingSouth()==true){
    return !isVerticalWall(x-1,y);
        }
if (ulisseFacingEast()==true){
	return !isHorizontalWall(x,y-1);
		}
if (ulisseFacingNorth()==true){
	return !isVerticalWall(x,y);
		}	
if (ulisseFacingWest()==true){
	return !isHorizontalWall(x,y);
		}	
return false;
}

/**
* A basic move of Ulisse: checks if he is facing east
* @return true if Ulisse is facing east
*/
public boolean ulisseFacingEast() {
if (ulisse.getOrientation()==0)
return true;
else
return false;
}

/**
* A basic move of Ulisse: checks if he is facing north
* @return true if Ulisse is facing north
*/
public boolean ulisseFacingNorth() {
if (ulisse.getOrientation()==1)
return true;
else
return false;
}

/**
* A basic move of Ulisse: checks if he is facing west
* @return true if Ulisse is facing west
*/
public boolean ulisseFacingWest() {
if (ulisse.getOrientation()==2)
return true;
else
return false;
}

/**
* A basic move of Ulisse: checks if he is facing south
* @return true if Ulisse is facing south
*/
public boolean ulisseFacingSouth() {
if (ulisse.getOrientation()==3)
return true;
else
return false;
}

/**
* Resizes the room. If new cells are added, they will be empty.
* Ulisse will be moved if it would remain outside the room.
* @param nWidth the new width of the room. Must be positive
* @param nHeight the new height of the room. Must be positive
*/

public void resize(int nWidth, int nHeight) {
	 
	 Cell oldCells[]=cells;
	 cells=new Cell[nWidth*nHeight];
	 for (int i=0;i<cells.length;i++){
		 cells[i]=new Cell();
		
	 }
       for (int y=0; y<getHeight() && y<nHeight; y++){
	       for (int x=0; x<getWidth() && x<nWidth; x++){
	    	   cells[y*nWidth+x]=oldCells[y*getWidth()+x];
	    	   if (getUlisseX()>= nWidth-1)
	      		   setUlisseX(nWidth-1);
	      	   if (getUlisseY()>=nHeight-1)
	      		   setUlisseY(nHeight-1);
	       }
	       
	       
	}
width=nWidth;
height=nHeight;
}

/**
* Places Ulisse in the cell with coordinates (x,y) where (0,0) is the lower
* left corner of the room.
* @param x new coordinate of Ulisse
* @param y new coordinate of Ulisse
*/
public void moveUlisseTo(int x, int y) {
	if (x>=0 && x<=width-1 && y>=0 && y<=height-1)
ulisse.setX(x);
ulisse.setY(y);
}

/**
* Increments the number of diamonds in cell (x,y)
*/
public void addDiamond(int x, int y) {
	if (x>=0 && x<=width-1 && y>=0 && y<=height-1)
            setDiamondsInCell(x, y, getNDiamondsInCell(x,y)+1);
}

/**
* Removes a diamond in cell (x,y)
* @return false if no diamond was present, true otherwise
*/
public boolean removeDiamond(int x, int y) {
	if (x>=0 && x<=width-1 && y>=0 && y<=height-1){
if (getNDiamondsInCell(x,y)==0)
return false;
else
setDiamondsInCell(x,y,getNDiamondsInCell(x,y)-1);
return true;
	}
	return false;

}

/**
* Sets the number of diamonds in cell (x,y)
* @param x coordinate of cell
* @param y coordinate of cell
* @param n number of diamonds
*/
public void setDiamondsInCell(int x, int y, int n) {
	if (x>=0 && x<=width-1 && y>=0 && y<=height-1)
        cellAt(x,y).setNDiamonds(n);

}

/**
* Places or removes an horizontal wall between cells (x,y) and (x,y+1)
* @param x coordinate
* @param y coordinate of the lower cell
* @param wall true to place a wall, false to remove
* @return true if the wall state has changed, false otherwise
*/
public boolean setHorizontalWall(int x, int y, boolean wall) {
	if (x>=0 && x<=width-1 && y>=0 && y<=height-1){
       cellAt(x,y).setHWall(wall);
       if(cellAt(x,y).getHWall()==wall)
       return false;
       else
       return true;
	}
	return false;
}



/**
* Places or removes a vertical wall between cells (x,y) and (x+1,y)
* @param x coordinate of left cell
* @param y coordinate
* @param wall true to place a wall, false to remove
* @return true if the wall state has changed, false otherwise
*/
public boolean setVerticalWall(int x, int y, boolean wall) {
	if (x>=0 && x<=width-1 && y>=0 && y<=height-1){
        cellAt(x,y).setVWall(wall);
        if(cellAt(x,y).getVWall()==wall)
        return false;
        else
        return true;
	}
	return false;
}

/**
* @return width of room (number of cells)
*/
public int getWidth(){
	   return width;
}


/**
* @return height of room (number of cells)
*/
public int getHeight(){
	   return height;
}


/**
* Gives the number of diamonds in the specified cell
* @param x x-coordinate of cell
* @param y y-coordinate of cell
* @return number of diamonds
*/
public int getNDiamondsInCell(int x, int y) {
	if (x>=0 && x<=width-1 && y>=0 && y<=height-1)
       return cellAt(x,y).getNDiamonds();
	else return 0;
}

/**
* Is there an horizontal wall between cells (x,y) and (x,y+1)?
* Set horizontal walls in the upper and the lower side of the room 
* @param x x coordinate of cell
* @param y y coordinate of lower cell
* @return true if a wall is present
*/
public boolean isHorizontalWall(int x, int y) {
	if (y>=getHeight()-1 || y<0) 
		return true;
	else {
		if (x>=0 && x<=width-1 && y>=0 && y<=height-1)
		    return cellAt(x,y).getHWall();
	}
	return false;
}

/**
* Is there a vertical wall between cells (x,y) and (x+1,y)?
* Set vertical walls in the left and the right side of the room
* @param x x coordinate of cell
* @param y y coordinate of left cell
* @return true if a wall is present
*/
public boolean isVerticalWall(int x, int y) {
	if (x>=getWidth()-1 || x<0)
		return true;
	else{
		if (x>=0 && x<=width-1 && y>=0 && y<=height-1)
		   return cellAt(x,y).getVWall();
	}
	return false;
}

/**
 * 
 * @param x coordinate of cell
 * @param y coordinate of cell
 * @return given the coordinates (x,y) of the cell, returns the position in the array
 */
public Cell cellAt(int x,int y){
        return cells[y*width+x];
	}

/** 
 * These methods are used to take and modify Ulisse's status
 */

int getUlisseBag() {
	return ulisse.getBag();
}

void setUlisseBag(int n) {
	ulisse.setBag(n);
}

int getUlisseX() {
	return ulisse.getX();
}
void setUlisseX(int xnew) {
	ulisse.setX(xnew);
	}

int getUlisseY() {
	return ulisse.getY();
}
void setUlisseY(int ynew) {
	ulisse.setY(ynew);
}

int getUlisseOrientation() {
	return ulisse.getOrientation();
}


void setUlisseOrientation(int o) {
	ulisse.setOrientation(o);
}
boolean getUlisseDied() {
	return ulisse.getDied();
}
void setUlisseDied(boolean flag) {
	ulisse.setDied(flag);
}


private int width;
private int height;
private Cell cells[];
private Ulisse ulisse;
}