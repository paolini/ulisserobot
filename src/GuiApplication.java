import java.awt.EventQueue;
import javax.swing.AbstractAction;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JSpinner;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseListener;
import java.awt.Component;
import java.awt.event.MouseEvent;
import javax.swing.JButton;
import java.awt.Dimension;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.io.Writer;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import javax.swing.SpinnerNumberModel;
import java.io.File;


public class GuiApplication {
    private JFrame frame;
    private JTextArea textArea;
    private World world;
    private GuiCanvas canvas;
    private JSplitPane splitPane;
    private JTabbedPane tabbedPane;
    private JPanel panel_1;
    private JPanel panel;
    private JPanel panel_2;
    private JButton btnTakeDiamonds;
    private JButton btnTurnLeft;
    private JButton btnTurnRight;
    private JButton btnDropDiamonds;
    private JButton btnAdvanced;
    private JLabel lblWidth;
    private JSpinner spinner;
    private JLabel lblHeight;
    private JSpinner spinner_1;
    private JButton btnRun;
    private JButton btnReset;
    private JMenuBar menuBar;
    private JMenu mnFile;
    private JMenuItem mntmNew;
    private JMenuItem mntmOpen;
    private JMenuItem mntmSave;
    private JMenuItem mntmExit;
    private VirtualMachine virtualMachine;
    
    public World getWorld(){
        return world;
    }
    /**
     * Launch the application.
     */
    public static void launch() {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    GuiApplication window = new GuiApplication();
                    window.frame.setVisible(true);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the apGuiApplicationion.
     */
    public GuiApplication() {
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
    	virtualMachine= new VirtualMachine();
    	world = new World();

    	canvas = new GuiCanvas(this);
        
        frame = new JFrame();
        frame.setBounds(100, 100, 711, 460);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        menuBar = new JMenuBar();
        frame.setJMenuBar(menuBar);
        
        JPopupMenu.setDefaultLightWeightPopupEnabled(false);
        
        mnFile = new JMenu("File");
        menuBar.add(mnFile);
       
        /**
         * Recreate the original world
         */

        mntmNew = new JMenuItem("New");
        mntmNew.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            world = new World();
            canvas.repaint();
            spinner.setModel(new SpinnerNumberModel(new Integer(world.getWidth()), new Integer(2), null, new Integer(1)));
            spinner_1.setModel(new SpinnerNumberModel(new Integer(world.getHeight()), new Integer(2), null, new Integer(1)));
            }
        });
        mnFile.add(mntmNew);
       
        /**
         * Open a file that contains a new world's scheme
         */

        mntmOpen = new JMenuItem("Open");
        mntmOpen.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		JFileChooser fileChooser = new JFileChooser();
        		fileChooser.setFileFilter(new WrdFileFilter());
        		int n = fileChooser.showOpenDialog(GuiApplication.this.frame);
        		if( n == JFileChooser.APPROVE_OPTION){
        			File file = fileChooser.getSelectedFile();
        			String NameFile;
        			NameFile=file.getName().toLowerCase();
					textArea.setText("");
        			if(NameFile.endsWith("wrd")){
        				WorldReader reader;
						try {
							reader = new WorldReader(new FileReader(file));
							reader.readWorld(world);
						} 
						catch (FileNotFoundException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						catch (ParseError e1) {
							// TODO Auto-generated catch block
				    		JOptionPane.showMessageDialog(frame, e1.toString(),"Error",JOptionPane.ERROR_MESSAGE);
						}
        			}
        			else{
        				BufferedReader reader;
						try {
							reader = new BufferedReader(new FileReader(file));
							String line = reader.readLine();
	        				while(line != null){
	        					textArea.append(line+'\n');
	        					line = reader.readLine();
	        				}
	        				reader.close();
						} 
						catch (FileNotFoundException e1) {
				    		JOptionPane.showMessageDialog(frame, "File Not Found","Error",JOptionPane.ERROR_MESSAGE);
							// TODO Auto-generated catch block
							e1.printStackTrace();
						} 
						catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
        			}      		
        		}
        	}
        });
        mnFile.add(mntmOpen);
       

        /**
         * Saves the world's scheme into a file that can be reopen afterward.
         */
        mntmSave = new JMenuItem("Save");
        mntmSave.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		JFileChooser fileChooser = new JFileChooser();
        		fileChooser.setFileFilter(new WrdFileFilter());
        		int n = fileChooser.showSaveDialog(GuiApplication.this.frame);
        		if( n == JFileChooser.APPROVE_OPTION){
        			File f = fileChooser.getSelectedFile();
					try {
						if(f.getName().endsWith(".wrd")){
							world.toStream(new FileWriter(f));	
							}
						else{
							BufferedWriter write = new BufferedWriter(new FileWriter(f));
							write.append(textArea.getText());
							write.close();
							}
						//write.flush();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
			}
		}
	    });
        mnFile.add(mntmSave);

        /**
         * Close the program.
         */
        
        mntmExit = new JMenuItem("Exit");
        mntmExit.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                        System.exit(0);
                }
        });
        mnFile.add(mntmExit);
        
 
        splitPane = new JSplitPane();
        splitPane.setAlignmentY(Component.BOTTOM_ALIGNMENT);
        splitPane.setDividerSize(5);
        GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
        groupLayout.setHorizontalGroup(
            groupLayout.createParallelGroup(Alignment.LEADING)
                .addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
                    .addGap(2)
                    .addComponent(splitPane, GroupLayout.DEFAULT_SIZE, 709, Short.MAX_VALUE))
        );
        groupLayout.setVerticalGroup(
            groupLayout.createParallelGroup(Alignment.TRAILING)
                .addComponent(splitPane, GroupLayout.PREFERRED_SIZE, 409, Short.MAX_VALUE)
        );
        
        canvas.setBackground(Color.WHITE);
        canvas.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
        canvas.setSize(new Dimension(301,301));
        canvas.setPreferredSize(new Dimension(10, 10));
        
        splitPane.setLeftComponent(canvas);
        frame.getContentPane().setLayout(groupLayout);   

        tabbedPane = new JTabbedPane(JTabbedPane.TOP);
        splitPane.setRightComponent(tabbedPane);
       
        /**
         * Create the panel "Play" where are the buttons to move Ulisse.
         */

        panel_1 = new JPanel();
        tabbedPane.addTab("Play", null, panel_1, null);
        
        /**
         * Create the button "Advance" that moves Ulisse Robot in the next cell.
         */       

        btnAdvanced = new JButton("Advance");
        btnAdvanced.addActionListener(new ActionListener() {

        	public void actionPerformed(ActionEvent arg0) {
                world.ulisseAdvance();
                UlisseDied();
                canvas.repaint();
            }
        });
        btnAdvanced.setHorizontalTextPosition(SwingConstants.CENTER);
     
        /**
         *Create the buttons "Turn Right" and "Turn Left", that rotate Ulisse Robot.
         */ 
        btnTurnRight = new JButton("Turn Right");
        btnTurnRight.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                world.ulisseTurnRight();
                canvas.repaint();
            }
        });
        
        btnTurnLeft = new JButton("Turn Left");
        btnTurnLeft.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                world.ulisseTurnLeft();
                canvas.repaint();
            }
        });
       
        /**
         * Create the button "Take diamonds", that allows Ulisse to take diamonds from the floor.
         */
        btnTakeDiamonds = new JButton("Take diamonds");
        btnTakeDiamonds.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                world.ulisseTakeDiamond();
                UlisseDied();
                canvas.repaint();
            }
        });
       
        /**
         * Create the button "Drop Diamonds", that allows Ulisse to drop diamonds on the floor.
         * Note that Ulisse has infinite diamonds.
         */

        btnDropDiamonds = new JButton("Drop Diamonds");
        btnDropDiamonds.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                world.ulisseDropDiamond();
                canvas.repaint();
            }
        });

        GroupLayout gl_panel_1 = new GroupLayout(panel_1);
        gl_panel_1.setHorizontalGroup(
            gl_panel_1.createParallelGroup(Alignment.LEADING)
                .addGroup(gl_panel_1.createSequentialGroup()
                    .addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
                        .addGroup(gl_panel_1.createSequentialGroup()
                            .addContainerGap()
                            .addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
                                .addComponent(btnTakeDiamonds, GroupLayout.DEFAULT_SIZE, 195, Short.MAX_VALUE)
                                .addComponent(btnTurnLeft, GroupLayout.DEFAULT_SIZE, 195, Short.MAX_VALUE))
                            .addGap(18)
                            .addGroup(gl_panel_1.createParallelGroup(Alignment.TRAILING)
                                .addComponent(btnTurnRight, GroupLayout.DEFAULT_SIZE, 160, Short.MAX_VALUE)
                                .addComponent(btnDropDiamonds, GroupLayout.DEFAULT_SIZE, 160, Short.MAX_VALUE)))
                        .addGroup(gl_panel_1.createSequentialGroup()
                            .addGap(134)
                            .addComponent(btnAdvanced, GroupLayout.DEFAULT_SIZE, 153, Short.MAX_VALUE)
                            .addGap(98)))
                    .addContainerGap())
        );
        gl_panel_1.setVerticalGroup(
            gl_panel_1.createParallelGroup(Alignment.TRAILING)
                .addGroup(Alignment.LEADING, gl_panel_1.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(btnAdvanced)
                    .addGap(60)
                    .addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
                        .addComponent(btnTurnLeft)
                        .addComponent(btnTurnRight))
                    .addGap(45)
                    .addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
                        .addComponent(btnTakeDiamonds)
                        .addComponent(btnDropDiamonds))
                    .addContainerGap(188, Short.MAX_VALUE))
        );
        panel_1.setLayout(gl_panel_1);
       
        /**
         * Create the panel "Edit" where you can resize the grid.
         */

        panel = new JPanel();
        tabbedPane.addTab("Edit", null, panel, null);

        spinner = new JSpinner();
        spinner.setModel(new SpinnerNumberModel(new Integer(10), new Integer(2), null, new Integer(1)));
        spinner.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent arg0) {
                int x=(Integer) spinner.getValue();
                int y=world.getHeight();
                world.resize(x, y);
                canvas.repaint();
            }
        });
        
        spinner_1 = new JSpinner();
        spinner_1.setModel(new SpinnerNumberModel(new Integer(10), new Integer(2), null, new Integer(1)));
        spinner_1.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                int y=(Integer) spinner_1.getValue();
                int x=world.getWidth();
                world.resize(x, y);
                canvas.repaint();
            }
        });
        
        lblWidth = new JLabel("Width");
        lblHeight = new JLabel("Height");
        
        GroupLayout gl_panel = new GroupLayout(panel);
        gl_panel.setHorizontalGroup(
                gl_panel.createParallelGroup(Alignment.LEADING)
                        .addGroup(gl_panel.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(lblWidth)
                                .addPreferredGap(ComponentPlacement.UNRELATED)
                                .addComponent(spinner, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addGap(38)
                                .addComponent(lblHeight)
                                .addPreferredGap(ComponentPlacement.UNRELATED)
                                .addComponent(spinner_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(147, Short.MAX_VALUE))
        );
        gl_panel.setVerticalGroup(
                gl_panel.createParallelGroup(Alignment.LEADING)
                        .addGroup(gl_panel.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(gl_panel.createParallelGroup(Alignment.TRAILING)
                                        .addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
                                                .addComponent(spinner, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                                .addComponent(lblHeight)
                                                .addComponent(spinner_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                        .addComponent(lblWidth))
                                .addContainerGap(253, Short.MAX_VALUE))
        );
        panel.setLayout(gl_panel);
       
        /**
         * Create the panel "Code", where you can insert a code and run it.
         */

        panel_2 = new JPanel();
        tabbedPane.addTab("Code", null, panel_2, null);

        textArea = new JTextArea();
        textArea.setText("main = {\n}\n");
            
        btnRun = new JButton("Run");
        btnRun.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String testo = textArea.getText();
                StringReader reader = new StringReader (testo);
                Compiler compiler = new Compiler();
                try{
                    Program program = compiler.compile(reader);
                    /**fai virtual machine*/
                    virtualMachine.setProgram(program);
                    virtualMachine.setWorld(world);
                    try{
                        while (!virtualMachine.hasTerminated())
                            virtualMachine.step();
                    }
                    catch(VirtualMachineError err){
                        JOptionPane.showMessageDialog(frame, "Errore nella Virtual Machine!\n"+err.toString());
                    }
                }    
                catch(ParseError err){
                    /**create error notification*/
                    String s = err.toString();
                    JOptionPane.showMessageDialog(frame, "Attenzione il codice che e' stato inserito non e' corretto!\n" + s,"Error",JOptionPane.ERROR_MESSAGE);
                }
                canvas.repaint();
            }
        });
        btnReset = new JButton("Reset");
        btnReset.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                textArea.setText("");
            }
        });
            
        GroupLayout gl_panel_2 = new GroupLayout(panel_2);
        gl_panel_2.setHorizontalGroup(
                gl_panel_2.createParallelGroup(Alignment.LEADING)
                        .addGroup(gl_panel_2.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(btnRun)
                                .addPreferredGap(ComponentPlacement.RELATED, 239, Short.MAX_VALUE)
                                .addComponent(btnReset)
                                .addGap(21))
                        .addGroup(gl_panel_2.createSequentialGroup()
                                .addComponent(textArea, GroupLayout.DEFAULT_SIZE, 396, Short.MAX_VALUE)
                                .addContainerGap())
        );
        gl_panel_2.setVerticalGroup(
                gl_panel_2.createParallelGroup(Alignment.LEADING)
                        .addGroup(gl_panel_2.createSequentialGroup()
                                .addComponent(textArea, GroupLayout.DEFAULT_SIZE, 519, Short.MAX_VALUE)
                                .addPreferredGap(ComponentPlacement.RELATED)
                                .addGroup(gl_panel_2.createParallelGroup(Alignment.TRAILING)
                                        .addGroup(gl_panel_2.createSequentialGroup()
                                                .addComponent(btnRun)
                                                .addGap(27))
                                        .addGroup(gl_panel_2.createSequentialGroup()
                                                .addComponent(btnReset)
                                                .addGap(26)))
                                .addGap(25))
        );
        panel_2.setLayout(gl_panel_2);          
        
        /**
         * Basing on where and how you click you have the following actions:
         * - left click near a line of the grid make a wall, right click remove wall if it's present;
         * - left click in the center of a cell adds a diamond, right click removes a diamond if it's present.
         */
        
        canvas.addMouseListener(new MouseListener() {
	    	public void mouseEntered(MouseEvent e) {
	    	}
	    	public void mouseClicked(MouseEvent e) {
	    		 int widthcell=canvas.getwidthcell();
	    		 int k = e.getButton();
	    		 int x = e.getX();
	    		 int y = world.getHeight()*widthcell - e.getY();
	    		 int c = 16;
	    		 x -= c/2;
	    		 y -= c/2;
	    		 int i = x/widthcell;
	    		 int j = y/widthcell;
	    		 int xx = x%widthcell;
	    		 int yy = y%widthcell;
	    		 if (xx <= widthcell-c && yy <= widthcell-c) {
	        		 if (k==1)
	        			 world.addDiamond(i, j);
	        		 if (k==3 && world.getNDiamondsInCell(i, j) != 0)
	        			 world.removeDiamond(i, j);
	        	 }
	    		 if (xx > widthcell-c && yy < widthcell-c) {
	        		world.setVerticalWall(i, j, k==1);
	    		 }
	    		 if (xx < widthcell-c && yy > widthcell-c) {
	         		world.setHorizontalWall(i, j, k==1);
	     		 }
	    		 canvas.repaint();
	    		 	
	    	}
			public void mouseExited(MouseEvent e) {
			}
			public void mousePressed(MouseEvent e) {
			}
			public void mouseReleased(MouseEvent e) {
			}
        });
    }
    /**
     * Check if Ulisse bangs against a wall.
     */
    private void UlisseDied(){
    	boolean died = world.getUlisseDied();
    	if(died == true){
    		JOptionPane.showMessageDialog(frame, "Game Over!","Error",JOptionPane.ERROR_MESSAGE);
    		System.exit(0);
    	}
    }
}