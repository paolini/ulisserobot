import java.io.Closeable;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.Writer;

/**
 * The WorldWriter class is used to write a World description
 * on a file (or more generally to an output stream)
 */

public class WorldWriter implements Closeable {
	/**
	 * prepare for writing on the given output stream
	 * @param out the stream where to write
	 */
	public WorldWriter(Writer some_out) {
		 out= new PrintWriter(some_out);
	}
	/**
	 * prepare for writing on the given file
	 * @param filename the name of the file which will be (over-)written
	 * @throws FileNotFoundException 
	 */
	public WorldWriter(String filename) throws FileNotFoundException {
		 out= new PrintWriter(filename);	
	}
	
	/**
	 * write the description of the world to the stream.
	 * After writing the World the WorldWriter must be closed.
	 * @param world the World object whose description is written to the file
	 */
	public void write(World world) {
		out.println("Width = " + world.getWidth());
		out.println("Height = " + world.getHeight());
		out.println("Ulisse_x = " + world.getUlisseX());
		out.println("Ulisse_y = " + world.getUlisseY());
		out.println("Diamonds= "+ world.getUlisseBag());
		out.println("Orientation = " + world.getUlisseOrientation());
		for (int j=0;j<world.getHeight();j++){
			for (int i=0; i< world.getWidth(); i++){
				out.println("("+i+","+j+")=("+world.getNDiamondsInCell(i,j)+","+world.isHorizontalWall(i,j)+","+world.isVerticalWall(i,j)+")");
			}
		}
		
		
			
		}
	/**
	 * close stream
	 */
	 public void close() {
		 out.close();
	 }
	 private PrintWriter out;
}
