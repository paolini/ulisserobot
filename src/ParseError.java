/**
 * This class represents an error occurred while parsing a file (in general any stream).
 * It has an error message, 
 * an optional line number (the line where the error occurred),
 * an optional file name (the name of the file being parsed)
 */
public class ParseError extends Exception {
	/**
	 * this is suggested by eclipse
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * create a new ParseError with a description attached to it
	 * @param message a message describing the error
	 */
    public ParseError(String message) {
    	this.message = message;
    }
	
	public ParseError(String message, int lineNumber) {
		this.message=message;
		this.lineNumber = lineNumber;
		}
	
	/**
	 * gives a complete string representation of the error,
	 * possibly completed with the line number where the error has occurred
	 * and the filename.
	 * @return something like: "Integer Expected while reading world size at line 12 in file 'newRoom.wrl'" 
	 */
	public String toString() {
		return "Error at line "+getLineNumber()+" in file "+getFileName()+": "+message; 
	}
	
	/**
	 * sets the message describing the error
	 * @param message 
	 */
	public void setMessage(String newMessage) {
		message=newMessage;		
	}
	
	/**
	 * gets the message describing the error
	 */
	public String getMessage() {
		return message;		
	}
	
	/**
	 * sets the line where the error occured
	 * @param lineNumber
	 */
	public void setLineNumber(int newLineNumber) {
		lineNumber=newLineNumber;
	}
	
	/**
	 * gets the number of the line where the error occurred
	 * @return the line number (1 for first line), 0 if not known
	 */
	public int getLineNumber() {
		return lineNumber; 		
	}
	
	/**
	 * sets the name of the file where the error occurred
	 * @param filename
	 */
	public void setFileName(String newFileName) {
		fileName=newFileName;
	}
	
	/**
	 * gets the name of the file where the error occurred
	 * @return the name of the file or null if unknown
	 */
	public String getFileName() {
		return fileName; 
	}
	
	private String message;
	private String fileName;
	private int lineNumber;

}
