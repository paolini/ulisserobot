/**
 * A simple VirtualMachine. 
 * 
 * It has:
 *  - a reference to a World object (the object being modified by builtin functions)
 *  - a reference to a Program (the program being executed)
 *  - a programCounter (the address of the next instruction in the program)
 *  - a single true/false "register" used for return values of builtin functions and conditional jumps
 *  - a VirtualMachineCallStack, i.e. a stack of addresses used by RETURN statements
 *  - a "flag" indicating if the machine has terminated execution
 *
 * TIPICAL USAGE:
 * 
 *   setWorld
 *   setProgram
 *   while(!hasTerminated)
 *      step
 *      
 */

public class VirtualMachine {	
	/**
	 * set the World to act on
	 * @param world is the given World
	 */
	
	World world;
	Program program;
	VirtualMachineCallStack VirtualMachineCallStack;
	
	private boolean running;
	private boolean register;
	private short programCounter; 
	
	public void setWorld(World newWorld) {

		world=newWorld;
	
	}
	
	/**
	 * sets a reference to the program to be executed
	 * and resets state
	 */
	public void setProgram(Program newProgram) {

		program=newProgram;
		reset();
	}
	
	 /**
	 * clear the stack 
	 * sets the state to: running
	 * sets the register to: false
	 * sets the programCounter to 0
	 */
	

	public void reset() {

		VirtualMachineCallStack= new VirtualMachineCallStack();
		
		running=true;
		register=false;
		programCounter=0;
	}
     /**
	 * has the program terminated?
	 * @return true if the program has terminated, false if not
	 */

	public boolean hasTerminated() {
		
		return !running;
	
	}
	 /**
	 * executes a single instruction of the program
	 * @throws VirtualMachineError if the instruction is not valid
	 */
	public void step() throws VirtualMachineError {

		short command=program.get(programCounter);
		programCounter++;
		switch(command){
		case 0: throw new VirtualMachineError ("The instruction is not valid");
			
		case 1: short builtin=program.get(programCounter);
			programCounter++;
//			"","advance","turnLeft","turnRight","front_is_clear","left_is_clear","right_is_clear",
//          "take_diamond","drop_diamond","diamond_is_present","facing_east","facing_north","facing_west","facing_south","true","false"};
			switch(builtin){
			case 1: world.ulisseAdvance();
			break;
			case 2 : world.ulisseTurnLeft();
			break;
			case 3 : world.ulisseTurnRight();
			break;
			case 4 : register=world.ulisseFrontIsClear();
			break;
			case 5 : register=world.ulisseLeftIsClear();
			break;
			case 6 : register=world.ulisseRightIsClear();
			break;
			case 7 : world.ulisseTakeDiamond();
			break;
			case 8 : world.ulisseDropDiamond();
			break;
			case 9 : register=world.ulisseDiamondIsPresent();
			break;
			case 10 : register=world.ulisseFacingEast();
			break;
			case 11 : register=world.ulisseFacingNorth();
			break;
			case 12 : register=world.ulisseFacingWest();
			break;
			case 13 : register=world.ulisseFacingSouth();
			break;
			case 14 : register=true;
			break;
			case 15 : register=false;
			break;
			default: throw new VirtualMachineError("invalid builtin");
			}
		break;
		case 2: short call=program.get(programCounter);
			programCounter++;
			VirtualMachineCallStack.push(programCounter);
				programCounter=call;
					break;
				
		case 3: 
    		if (VirtualMachineCallStack.isEmpty())
	    		running=false;
    		else
    			programCounter=VirtualMachineCallStack.pop();
			break;
		
		case 4: short jump=program.get(programCounter);
				programCounter=jump;
					break;
			
		case 5: short jumpTrue=program.get(programCounter);
			if (register==true){
				programCounter=jumpTrue;
			} else {
				programCounter++;
			}
					break;
		
		case 6: short jumpFalse=program.get(programCounter);
			if (register==false){
				programCounter=jumpFalse;
			} else {
				programCounter++;
			}
					break;
		}

	}
}


